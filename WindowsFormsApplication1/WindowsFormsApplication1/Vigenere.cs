﻿using System;

namespace Cryptography
{
	class Vigenere : ICrypt
	{
		private Util util = new Util();

		/**
		 * Получение полного ключа вижинера
		 * @param  {String} str
		 * @param  {String} key
		 * @return {String}
		 *
		 * @private
		 */
		private string getFullKey(string str, string key)
		{
			string tmpKey = "";

			char[] buffer = key.ToCharArray();
			int bufferLen = key.Length; // Длина ключа
			int len = str.Length; // Длина кода

			int counter = 0;
			while (counter < len)
			{
				tmpKey += buffer[(counter % bufferLen)];
				counter++;
			}

			return tmpKey;
		}

		/**
		 * Кодирование шифром
		 * @param  {String}     str
		 * @param  {String}     key
		 * @return {String}
		 *
		 * @public
		 */
		public string encode(string str, string key)
		{
			int codeStr, codeKey, shift;
			char tmpChar;

			string fullKey = getFullKey(str, key);

			string endStr = "";
			char[] bufferStr = str.ToCharArray();
			char[] bufferKey = fullKey.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				codeStr = (int)bufferStr[i];
				codeKey = (int)bufferKey[i];

				shift = util.getCharShift(codeKey);

				tmpChar = Convert.ToChar(util.shift(codeStr, shift));
				endStr += tmpChar;
			}

			return endStr;
		}

		/**
		 * Декодирование шифром
		 * @param  {String}     str
		 * @param  {String}     key
		 * @return {String}
		 *
		 * @public
		 */
		public string decode(string str, string key)
		{
			int codeStr, codeKey, shift;
			char tmpChar;

			string fullKey = getFullKey(str, key);

			string endStr = "";
			char[] bufferStr = str.ToCharArray();
			char[] bufferKey = fullKey.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				codeStr = (int)bufferStr[i];
				codeKey = (int)bufferKey[i];

				shift = util.getCharShift(codeKey);

				tmpChar = Convert.ToChar(util.shift(codeStr, -1 * shift));
				endStr += tmpChar;
			}

			return endStr;
		}

		public string Crypt(string text, string param)
		{
			return encode(text, param);
		}
		public string Decrypt(string text, string param)
		{
			return decode(text, param);
		}
	}
}

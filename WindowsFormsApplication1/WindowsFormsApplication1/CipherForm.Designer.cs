﻿namespace WindowsFormsApplication1
{
	partial class CipherForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CipherForm));
			this.panelMenu = new System.Windows.Forms.Panel();
			this.buttonAbout = new System.Windows.Forms.Button();
			this.buttonVigenere = new System.Windows.Forms.Button();
			this.buttonAtbash = new System.Windows.Forms.Button();
			this.buttonAffine = new System.Windows.Forms.Button();
			this.buttonCaesar = new System.Windows.Forms.Button();
			this.panelAbout = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.labelDescription = new System.Windows.Forms.Label();
			this.labelDescriptionTitle = new System.Windows.Forms.Label();
			this.labelAuthor = new System.Windows.Forms.Label();
			this.labelAuthorTitle = new System.Windows.Forms.Label();
			this.labelAboutTitle = new System.Windows.Forms.Label();
			this.buttonAboutBack = new System.Windows.Forms.Button();
			this.panelCipher = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.labelCipherParam = new System.Windows.Forms.Label();
			this.labelCipherResult = new System.Windows.Forms.Label();
			this.labelCipherText = new System.Windows.Forms.Label();
			this.labelCipherTitle = new System.Windows.Forms.Label();
			this.textBoxCipherParam = new System.Windows.Forms.TextBox();
			this.buttonCipherBack = new System.Windows.Forms.Button();
			this.buttonCipherDecrypt = new System.Windows.Forms.Button();
			this.buttonCipherCrypt = new System.Windows.Forms.Button();
			this.richTextBoxCipherResult = new System.Windows.Forms.RichTextBox();
			this.richTextBoxCipherText = new System.Windows.Forms.RichTextBox();
			this.panelMenu.SuspendLayout();
			this.panelAbout.SuspendLayout();
			this.panelCipher.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelMenu
			// 
			this.panelMenu.Controls.Add(this.buttonAbout);
			this.panelMenu.Controls.Add(this.buttonVigenere);
			this.panelMenu.Controls.Add(this.buttonAtbash);
			this.panelMenu.Controls.Add(this.buttonAffine);
			this.panelMenu.Controls.Add(this.buttonCaesar);
			this.panelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelMenu.Location = new System.Drawing.Point(0, 0);
			this.panelMenu.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.panelMenu.Name = "panelMenu";
			this.panelMenu.Size = new System.Drawing.Size(884, 661);
			this.panelMenu.TabIndex = 0;
			// 
			// buttonAbout
			// 
			this.buttonAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonAbout.Location = new System.Drawing.Point(287, 490);
			this.buttonAbout.Name = "buttonAbout";
			this.buttonAbout.Size = new System.Drawing.Size(310, 32);
			this.buttonAbout.TabIndex = 4;
			this.buttonAbout.Text = "об авторе";
			this.buttonAbout.UseVisualStyleBackColor = true;
			this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
			// 
			// buttonVigenere
			// 
			this.buttonVigenere.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonVigenere.Location = new System.Drawing.Point(287, 235);
			this.buttonVigenere.Name = "buttonVigenere";
			this.buttonVigenere.Size = new System.Drawing.Size(310, 32);
			this.buttonVigenere.TabIndex = 3;
			this.buttonVigenere.Text = "Vigenere";
			this.buttonVigenere.UseVisualStyleBackColor = true;
			this.buttonVigenere.Click += new System.EventHandler(this.buttonVigenere_Click);
			// 
			// buttonAtbash
			// 
			this.buttonAtbash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonAtbash.Location = new System.Drawing.Point(287, 184);
			this.buttonAtbash.Name = "buttonAtbash";
			this.buttonAtbash.Size = new System.Drawing.Size(310, 32);
			this.buttonAtbash.TabIndex = 2;
			this.buttonAtbash.Text = "Atbash";
			this.buttonAtbash.UseVisualStyleBackColor = true;
			this.buttonAtbash.Click += new System.EventHandler(this.buttonAtbash_Click);
			// 
			// buttonAffine
			// 
			this.buttonAffine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonAffine.Location = new System.Drawing.Point(287, 132);
			this.buttonAffine.Name = "buttonAffine";
			this.buttonAffine.Size = new System.Drawing.Size(310, 32);
			this.buttonAffine.TabIndex = 1;
			this.buttonAffine.Text = "Affine";
			this.buttonAffine.UseVisualStyleBackColor = true;
			this.buttonAffine.Click += new System.EventHandler(this.buttonAffine_Click);
			// 
			// buttonCaesar
			// 
			this.buttonCaesar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonCaesar.Location = new System.Drawing.Point(287, 84);
			this.buttonCaesar.Name = "buttonCaesar";
			this.buttonCaesar.Size = new System.Drawing.Size(310, 32);
			this.buttonCaesar.TabIndex = 0;
			this.buttonCaesar.Text = "Caesar";
			this.buttonCaesar.UseVisualStyleBackColor = true;
			this.buttonCaesar.Click += new System.EventHandler(this.buttonCaesar_Click);
			// 
			// panelAbout
			// 
			this.panelAbout.Controls.Add(this.label1);
			this.panelAbout.Controls.Add(this.labelDescription);
			this.panelAbout.Controls.Add(this.labelDescriptionTitle);
			this.panelAbout.Controls.Add(this.labelAuthor);
			this.panelAbout.Controls.Add(this.labelAuthorTitle);
			this.panelAbout.Controls.Add(this.labelAboutTitle);
			this.panelAbout.Controls.Add(this.buttonAboutBack);
			this.panelAbout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelAbout.Location = new System.Drawing.Point(0, 0);
			this.panelAbout.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.panelAbout.Name = "panelAbout";
			this.panelAbout.Size = new System.Drawing.Size(884, 661);
			this.panelAbout.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(36, 167);
			this.label1.Margin = new System.Windows.Forms.Padding(0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(800, 2);
			this.label1.TabIndex = 6;
			// 
			// labelDescription
			// 
			this.labelDescription.AutoSize = true;
			this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDescription.Location = new System.Drawing.Point(98, 225);
			this.labelDescription.Name = "labelDescription";
			this.labelDescription.Size = new System.Drawing.Size(172, 20);
			this.labelDescription.TabIndex = 5;
			this.labelDescription.Text = "Описание программы";
			// 
			// labelDescriptionTitle
			// 
			this.labelDescriptionTitle.AutoSize = true;
			this.labelDescriptionTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelDescriptionTitle.Location = new System.Drawing.Point(70, 192);
			this.labelDescriptionTitle.Name = "labelDescriptionTitle";
			this.labelDescriptionTitle.Size = new System.Drawing.Size(100, 24);
			this.labelDescriptionTitle.TabIndex = 4;
			this.labelDescriptionTitle.Text = "Описание";
			// 
			// labelAuthor
			// 
			this.labelAuthor.AutoSize = true;
			this.labelAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAuthor.Location = new System.Drawing.Point(88, 119);
			this.labelAuthor.Name = "labelAuthor";
			this.labelAuthor.Size = new System.Drawing.Size(274, 20);
			this.labelAuthor.TabIndex = 3;
			this.labelAuthor.Text = "Иван Иванов Иванович. 2 Курс СГУ\r\n";
			// 
			// labelAuthorTitle
			// 
			this.labelAuthorTitle.AutoSize = true;
			this.labelAuthorTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAuthorTitle.Location = new System.Drawing.Point(60, 86);
			this.labelAuthorTitle.Name = "labelAuthorTitle";
			this.labelAuthorTitle.Size = new System.Drawing.Size(66, 24);
			this.labelAuthorTitle.TabIndex = 2;
			this.labelAuthorTitle.Text = "Автор";
			// 
			// labelAboutTitle
			// 
			this.labelAboutTitle.AutoSize = true;
			this.labelAboutTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelAboutTitle.Location = new System.Drawing.Point(368, 41);
			this.labelAboutTitle.Name = "labelAboutTitle";
			this.labelAboutTitle.Size = new System.Drawing.Size(108, 24);
			this.labelAboutTitle.TabIndex = 1;
			this.labelAboutTitle.Text = "Об Авторе";
			// 
			// buttonAboutBack
			// 
			this.buttonAboutBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonAboutBack.Location = new System.Drawing.Point(749, 602);
			this.buttonAboutBack.Name = "buttonAboutBack";
			this.buttonAboutBack.Size = new System.Drawing.Size(110, 25);
			this.buttonAboutBack.TabIndex = 0;
			this.buttonAboutBack.Text = "Назад";
			this.buttonAboutBack.UseVisualStyleBackColor = true;
			this.buttonAboutBack.Click += new System.EventHandler(this.buttonAboutBack_Click);
			// 
			// panelCipher
			// 
			this.panelCipher.Controls.Add(this.label6);
			this.panelCipher.Controls.Add(this.labelCipherParam);
			this.panelCipher.Controls.Add(this.labelCipherResult);
			this.panelCipher.Controls.Add(this.labelCipherText);
			this.panelCipher.Controls.Add(this.labelCipherTitle);
			this.panelCipher.Controls.Add(this.textBoxCipherParam);
			this.panelCipher.Controls.Add(this.buttonCipherBack);
			this.panelCipher.Controls.Add(this.buttonCipherDecrypt);
			this.panelCipher.Controls.Add(this.buttonCipherCrypt);
			this.panelCipher.Controls.Add(this.richTextBoxCipherResult);
			this.panelCipher.Controls.Add(this.richTextBoxCipherText);
			this.panelCipher.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelCipher.Location = new System.Drawing.Point(0, 0);
			this.panelCipher.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.panelCipher.Name = "panelCipher";
			this.panelCipher.Size = new System.Drawing.Size(884, 661);
			this.panelCipher.TabIndex = 2;
			this.panelCipher.VisibleChanged += new System.EventHandler(this.panelCipher_VisibleChanged);
			// 
			// label6
			// 
			this.label6.BackColor = System.Drawing.Color.Black;
			this.label6.Location = new System.Drawing.Point(420, 70);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(2, 530);
			this.label6.TabIndex = 10;
			// 
			// labelCipherParam
			// 
			this.labelCipherParam.AutoSize = true;
			this.labelCipherParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCipherParam.Location = new System.Drawing.Point(24, 465);
			this.labelCipherParam.Name = "labelCipherParam";
			this.labelCipherParam.Size = new System.Drawing.Size(149, 24);
			this.labelCipherParam.TabIndex = 9;
			this.labelCipherParam.Text = "Сила смещения";
			// 
			// labelCipherResult
			// 
			this.labelCipherResult.AutoSize = true;
			this.labelCipherResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCipherResult.Location = new System.Drawing.Point(439, 80);
			this.labelCipherResult.Name = "labelCipherResult";
			this.labelCipherResult.Size = new System.Drawing.Size(103, 24);
			this.labelCipherResult.TabIndex = 8;
			this.labelCipherResult.Text = "Результат";
			// 
			// labelCipherText
			// 
			this.labelCipherText.AutoSize = true;
			this.labelCipherText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCipherText.Location = new System.Drawing.Point(28, 80);
			this.labelCipherText.Name = "labelCipherText";
			this.labelCipherText.Size = new System.Drawing.Size(370, 24);
			this.labelCipherText.TabIndex = 7;
			this.labelCipherText.Text = "Текст для шифрования / дешифрования";
			// 
			// labelCipherTitle
			// 
			this.labelCipherTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.labelCipherTitle.Location = new System.Drawing.Point(300, 16);
			this.labelCipherTitle.Name = "labelCipherTitle";
			this.labelCipherTitle.Size = new System.Drawing.Size(242, 25);
			this.labelCipherTitle.TabIndex = 6;
			this.labelCipherTitle.Text = "Алгоритм Шифрования";
			this.labelCipherTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBoxCipherParam
			// 
			this.textBoxCipherParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBoxCipherParam.Location = new System.Drawing.Point(28, 496);
			this.textBoxCipherParam.Name = "textBoxCipherParam";
			this.textBoxCipherParam.Size = new System.Drawing.Size(372, 29);
			this.textBoxCipherParam.TabIndex = 5;
			this.textBoxCipherParam.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCipherParam_KeyPress);
			// 
			// buttonCipherBack
			// 
			this.buttonCipherBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonCipherBack.Location = new System.Drawing.Point(749, 602);
			this.buttonCipherBack.Name = "buttonCipherBack";
			this.buttonCipherBack.Size = new System.Drawing.Size(110, 25);
			this.buttonCipherBack.TabIndex = 4;
			this.buttonCipherBack.Text = "Назад";
			this.buttonCipherBack.UseVisualStyleBackColor = true;
			this.buttonCipherBack.Click += new System.EventHandler(this.buttonCipherBack_Click);
			// 
			// buttonCipherDecrypt
			// 
			this.buttonCipherDecrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonCipherDecrypt.Location = new System.Drawing.Point(254, 552);
			this.buttonCipherDecrypt.Name = "buttonCipherDecrypt";
			this.buttonCipherDecrypt.Size = new System.Drawing.Size(146, 35);
			this.buttonCipherDecrypt.TabIndex = 3;
			this.buttonCipherDecrypt.Text = "Дешифровать";
			this.buttonCipherDecrypt.UseVisualStyleBackColor = true;
			this.buttonCipherDecrypt.Click += new System.EventHandler(this.buttonCipherDecrypt_Click);
			// 
			// buttonCipherCrypt
			// 
			this.buttonCipherCrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.buttonCipherCrypt.Location = new System.Drawing.Point(28, 552);
			this.buttonCipherCrypt.Name = "buttonCipherCrypt";
			this.buttonCipherCrypt.Size = new System.Drawing.Size(146, 35);
			this.buttonCipherCrypt.TabIndex = 2;
			this.buttonCipherCrypt.Text = "Шифровать";
			this.buttonCipherCrypt.UseVisualStyleBackColor = true;
			this.buttonCipherCrypt.Click += new System.EventHandler(this.buttonCipherCrypt_Click);
			// 
			// richTextBoxCipherResult
			// 
			this.richTextBoxCipherResult.Enabled = false;
			this.richTextBoxCipherResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.richTextBoxCipherResult.Location = new System.Drawing.Point(443, 113);
			this.richTextBoxCipherResult.Name = "richTextBoxCipherResult";
			this.richTextBoxCipherResult.Size = new System.Drawing.Size(404, 424);
			this.richTextBoxCipherResult.TabIndex = 1;
			this.richTextBoxCipherResult.Text = "";
			// 
			// richTextBoxCipherText
			// 
			this.richTextBoxCipherText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.richTextBoxCipherText.Location = new System.Drawing.Point(28, 113);
			this.richTextBoxCipherText.Name = "richTextBoxCipherText";
			this.richTextBoxCipherText.Size = new System.Drawing.Size(372, 330);
			this.richTextBoxCipherText.TabIndex = 0;
			this.richTextBoxCipherText.Text = "";
			// 
			// CipherForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 661);
			this.Controls.Add(this.panelCipher);
			this.Controls.Add(this.panelAbout);
			this.Controls.Add(this.panelMenu);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
			this.Name = "CipherForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CipherForm";
			this.Load += new System.EventHandler(this.CipherForm_Load);
			this.panelMenu.ResumeLayout(false);
			this.panelAbout.ResumeLayout(false);
			this.panelAbout.PerformLayout();
			this.panelCipher.ResumeLayout(false);
			this.panelCipher.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panelMenu;
		private System.Windows.Forms.Panel panelAbout;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelDescription;
		private System.Windows.Forms.Label labelDescriptionTitle;
		private System.Windows.Forms.Label labelAuthor;
		private System.Windows.Forms.Label labelAuthorTitle;
		private System.Windows.Forms.Label labelAboutTitle;
		private System.Windows.Forms.Button buttonAboutBack;
		private System.Windows.Forms.Panel panelCipher;
		private System.Windows.Forms.Button buttonAbout;
		private System.Windows.Forms.Button buttonVigenere;
		private System.Windows.Forms.Button buttonAtbash;
		private System.Windows.Forms.Button buttonAffine;
		private System.Windows.Forms.Button buttonCaesar;
		private System.Windows.Forms.RichTextBox richTextBoxCipherResult;
		private System.Windows.Forms.RichTextBox richTextBoxCipherText;
		private System.Windows.Forms.TextBox textBoxCipherParam;
		private System.Windows.Forms.Button buttonCipherDecrypt;
		private System.Windows.Forms.Button buttonCipherCrypt;
		private System.Windows.Forms.Button buttonCipherBack;
		private System.Windows.Forms.Label labelCipherTitle;
		private System.Windows.Forms.Label labelCipherText;
		private System.Windows.Forms.Label labelCipherResult;
		private System.Windows.Forms.Label labelCipherParam;
		private System.Windows.Forms.Label label6;
	}
}


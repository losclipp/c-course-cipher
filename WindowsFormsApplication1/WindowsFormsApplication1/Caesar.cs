﻿using System;

namespace Cryptography
{
	class Caesar : ICrypt
	{
		private Util util = new Util();

		/**
		 * Кодирование шифром
		 * @param  {String}     str
		 * @param  {Int}        shift
		 * @return {String}
		 *
		 * @public
		 */
		public string encode(string str, int shift)
		{
			int code;
			char tmpChar;

			string endStr = "";
			char[] buffer = str.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				code = (int)buffer[i];

				tmpChar = Convert.ToChar(util.shift(code, shift));
				endStr += tmpChar;
			}

			return endStr;
		}

		/**
		 * Декодирование шифром
		 * @param  {String}     str
		 * @param  {Int}        shift
		 * @return {String}
		 *
		 * @public
		 */
		public string decode(string str, int shift)
		{
			int code;
			char tmpChar;

			string endStr = "";
			char[] buffer = str.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				code = (int)buffer[i];

				tmpChar = Convert.ToChar(util.shift(code, -1 * shift));
				endStr += tmpChar;
			}

			return endStr;
		}

		public string Crypt(string text, string param)
		{
			int shift;
			try
			{
				shift = int.Parse(param);
			}
			catch
			{
				shift = 1;
			}
			return encode(text, shift);
		}
		public string Decrypt(string text, string param)
		{

			int shift;
			try
			{
				shift = int.Parse(param);
			}
			catch
			{
				shift = 1;
			}
			return decode(text, shift);
		}

	}
}

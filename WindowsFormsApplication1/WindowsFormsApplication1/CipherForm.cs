﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Cryptography;

namespace WindowsFormsApplication1
{
	enum PanelType { About, Cipher, Menu };

	public partial class CipherForm : Form
	{
		private ICrypt crypt = null;
		private bool CiphetParamOnlyDigit = false;

		public CipherForm()
		{
			InitializeComponent();
		}
		
		private void CipherForm_Load(object sender, EventArgs e)
		{
			panelAbout.Hide();
			panelCipher.Hide();
			panelMenu.Show();
		}
		
		// Common Function
		private void ShowPanel(PanelType panelType)
		{
			panelAbout.Hide();
			panelCipher.Hide();
			panelMenu.Hide();

			switch (panelType)
			{
				case PanelType.About:
					{
						panelAbout.Show();
						break;
					}
				case PanelType.Cipher:
					{
						panelCipher.Show();
						break;
					}
				case PanelType.Menu:
					{
						panelMenu.Show();
						break;
					}
				default:
					{
						MessageBox.Show("Unknown PanelType" , "Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
						break;
					}
			}
		}

		
		// Author Panel Events
		private void buttonAboutBack_Click(object sender, EventArgs e)
		{
			ShowPanel(PanelType.Menu);
		}

				
		// Menu Panel Events
		private void buttonVigenere_Click(object sender, EventArgs e)
		{
			crypt = new Vigenere();
			ShowPanel(PanelType.Cipher);
		}
		private void buttonAtbash_Click(object sender, EventArgs e)
		{
			crypt = new Atbash();
			ShowPanel(PanelType.Cipher);
		}
		private void buttonAffine_Click(object sender, EventArgs e)
		{
			crypt = new Affine();
			ShowPanel(PanelType.Cipher);
		}
		private void buttonCaesar_Click(object sender, EventArgs e)
		{
			crypt = new Caesar();
			ShowPanel(PanelType.Cipher);
		}
		private void buttonAbout_Click(object sender, EventArgs e)
		{
			ShowPanel(PanelType.About);
		}

	
		// Cipher Panel Events
		private void panelCipher_VisibleChanged(object sender, EventArgs e)
		{
			EraseCipherPanel();

			if (panelCipher.Visible == true)
			{
				if (crypt.GetType() == typeof(Caesar))
				{
					labelCipherParam.Visible = true;
					labelCipherParam.Text = "Сила натяжения";

					textBoxCipherParam.Visible = true;
					CiphetParamOnlyDigit = true;

					richTextBoxCipherText.Height = 330;
				}
				else if (crypt.GetType() == typeof(Vigenere))
				{
					labelCipherParam.Visible = true;
					labelCipherParam.Text = "Ключ";

					textBoxCipherParam.Visible = true;
					CiphetParamOnlyDigit = false;

					richTextBoxCipherText.Height = 330;
				}
				else
				{
					labelCipherParam.Visible = false;
					textBoxCipherParam.Visible = false;

					richTextBoxCipherText.Height = 424;
				}
			}

			if (crypt != null)
				labelCipherTitle.Text = crypt.GetType().Name;
			else
				labelCipherTitle.Text = "Алгоритм шифрования";
		}
		private void EraseCipherPanel()
		{
			richTextBoxCipherText.Text = string.Empty;
			richTextBoxCipherResult.Text = string.Empty;
			textBoxCipherParam.Text = string.Empty;
		}
		private void buttonCipherCrypt_Click(object sender, EventArgs e)
		{
			string result = crypt.Crypt(richTextBoxCipherText.Text, textBoxCipherParam.Text);
			richTextBoxCipherResult.Text = result;
		}
		private void buttonCipherDecrypt_Click(object sender, EventArgs e)
		{
			string result = crypt.Decrypt(richTextBoxCipherText.Text, textBoxCipherParam.Text);
			richTextBoxCipherResult.Text = result;
		}
		private void textBoxCipherParam_KeyPress(object sender, KeyPressEventArgs e)
		{
			if(CiphetParamOnlyDigit)
				if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
				{
					e.Handled = true;
				}
		}
		private void buttonCipherBack_Click(object sender, EventArgs e)
		{
			ShowPanel(PanelType.Menu);
		}

	}

	
}

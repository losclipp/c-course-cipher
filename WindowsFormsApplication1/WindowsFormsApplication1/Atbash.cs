﻿using System;

namespace Cryptography
{
	class Atbash : ICrypt
	{
		private Util util = new Util();

		/**
		 * Кодирование шифром
		 * @param  {String}     str
		 * @return {String}
		 *
		 * @public
		 */
		public string encode(string str)
		{
			int code;
			char tmpChar;

			string endStr = "";
			char[] buffer = str.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				code = (int)buffer[i];

				tmpChar = Convert.ToChar(util.invert(code, 0));
				endStr += tmpChar;
			}

			return endStr;
		}

		/**
		 * Декодирование шифром
		 * @param  {String}     str
		 * @return {String}
		 *
		 * @public
		 */
		public string decode(string str)
		{
			int code;
			char tmpChar;

			string endStr = "";
			char[] buffer = str.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				code = (int)buffer[i];

				tmpChar = Convert.ToChar(util.invert(code, 0));
				endStr += tmpChar;
			}

			return endStr;
		}

		public string Crypt(string text, string param)
		{
			return encode(text);
		}
		public string Decrypt(string text, string param)
		{
			return decode(text);
		}
	}
}

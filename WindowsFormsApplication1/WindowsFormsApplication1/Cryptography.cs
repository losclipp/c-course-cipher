using System;

/**
 * Алгоритмы шифрования и дешифрования данных (Цезарь, Атбаш, Виженер и др.)
 */
namespace Cryptography
{
	class Util
	{
		// UTF8 коды букв
		// Английский алфавит
		public const int ENG_U_START = (int)'a';
		public const int ENG_U_END = (int)'z';
		public const int ENG_L_START = (int)'A';
		public const int ENG_L_END = (int)'Z';

		// Русский алфавит
		public const int RUS_U_START = (int)'а';
		public const int RUS_U_END = (int)'я';
		public const int RUS_L_START = (int)'А';
		public const int RUS_L_END = (int)'Я';

		/**
		 * Проверка на допустимость UTF8 кода сивола
		 * @param  {Integer}  code - UTF8 код
		 * @return {Boolean}
		 *
		 * @private
		 */
		private bool _isAvailable(int code)
		{
			bool isAvailable = false;
			if (inRange(ENG_U_START, code, ENG_U_END) || inRange(ENG_L_START, code, ENG_L_END))
			{
				isAvailable = true;
			}
			else if (inRange(RUS_U_START, code, RUS_U_END) || inRange(RUS_L_START, code, RUS_L_END))
			{
				isAvailable = true;
			}

			return isAvailable;
		}

		/**
		 * Проверка на английский алфавит
		 * @param  {Integer}  code - UTF8 код
		 * @return {Boolean}
		 *
		 * @private
		 */
		private bool _isEng(int code)
		{
			bool isEng = false;
			if (inRange(ENG_U_START, code, ENG_U_END) || inRange(ENG_L_START, code, ENG_L_END))
			{
				isEng = true;
			}

			return isEng;
		}

		/**
		 * Проверка на заглавную букву алфавит
		 * @param  {Integer}  code - UTF8 код
		 * @return {Boolean}
		 *
		 * @private
		 */
		private bool _isBig(int code)
		{
			bool isEng = _isEng(code);
			bool isBig = false;

			if (isEng)
			{
				if (inRange(ENG_U_START, code, ENG_U_END))
				{
					isBig = true;
				}
			}
			else {
				if (inRange(RUS_U_START, code, RUS_U_END))
				{
					isBig = true;
				}
			}

			return isBig;
		}

		/**
		 * Проверка на вхождение a2 параметра в диапазон между a1 - a3 (включительно).
		 * @param  {Integer}  a1
		 * @param  {Integer}  a2
		 * @param  {Integer}  a3
		 * @return {Boolean}
		 *
		 * @public
		 */
		public bool inRange(int a1, int a2, int a3)
		{
			if (a1 <= a2 && a2 <= a3)
			{
				return true;
			}
			else {
				return false;
			}
		}

		/**
		 * Смешение UTF8 кода в диапазоне алфавита.
		 * @param  {Integer} code - UTF8 код
		 * @param  {Integer} n    - Сила смещения
		 * @return {Integer}
		 *
		 * @public
		 */
		public int shift(int code, int n)
		{
			int min, max, range;
			int tmpCode;

			bool isAvailable = false;
			bool isEng = false;
			bool isBig = false;

			isAvailable = _isAvailable(code);

			// Недопустимый символ
			if (!isAvailable) { return code; };

			isEng = _isEng(code);
			isBig = _isBig(code);

			// Начало и конец
			if (isEng)
			{
				if (isBig)
				{
					min = ENG_U_START;
					max = ENG_U_END;
				}
				else {
					min = ENG_L_START;
					max = ENG_L_END;
				}
			}
			else {
				if (isBig)
				{
					min = RUS_U_START;
					max = RUS_U_END;
				}
				else {
					min = RUS_L_START;
					max = RUS_L_END;
				}
			}

			range = max - min + 1;
			n %= range;

			// Смещение
			tmpCode = code + n;

			// Выравнивание
			while (tmpCode < min)
			{
				tmpCode += range;
			}
			while (tmpCode > max)
			{
				tmpCode -= range;
			}

			return tmpCode;
		}

		/**
		 * Инвертирования UTF8 кода в диапазоне алфавита.
		 * @param  {Integer} code - UTF8 код
		 * @param  {Integer} n    - Сила смещения
		 * @return {Integer}
		 *
		 * @public
		 */
		public int invert(int code, int n)
		{
			int min, max, range;
			int tmpCode;

			bool isAvailable = false;
			bool isEng = false;
			bool isBig = false;

			isAvailable = _isAvailable(code);

			// Недопустимый символ
			if (!isAvailable) { return code; };

			isEng = _isEng(code);
			isBig = _isBig(code);

			// Начало и конец
			if (isEng)
			{
				if (isBig)
				{
					min = ENG_U_START;
					max = ENG_U_END;
				}
				else {
					min = ENG_L_START;
					max = ENG_L_END;
				}
			}
			else {
				if (isBig)
				{
					min = RUS_U_START;
					max = RUS_U_END;
				}
				else {
					min = RUS_L_START;
					max = RUS_L_END;
				}
			}

			range = max - min + 1;
			n %= range;

			// Смещение
			tmpCode = code - min;
			tmpCode = max - tmpCode;
			tmpCode = tmpCode + n;

			// Выравнивание
			while (tmpCode < min)
			{
				tmpCode += range;
			}
			while (tmpCode > max)
			{
				tmpCode -= range;
			}

			return tmpCode;
		}

		/**
		 * Получение порядкового номера UTF8 кода в диапазоне алфавита.
		 * @param  {Integer} code - UTF8 код
		 * @return {Integer}
		 *
		 * @public
		 */
		public int getCharShift(int code)
		{
			int min;
			int tmpCode;

			bool isAvailable = false;
			bool isEng = false;
			bool isBig = false;

			isAvailable = _isAvailable(code);

			// Недопустимый символ
			if (!isAvailable) { return code; };

			isEng = _isEng(code);
			isBig = _isBig(code);

			// Начало и конец
			if (isEng)
			{
				if (isBig)
				{
					min = ENG_U_START;
				}
				else {
					min = ENG_L_START;
				}
			}
			else {
				if (isBig)
				{
					min = RUS_U_START;
				}
				else {
					min = RUS_L_START;
				}
			}

			// Смещение
			tmpCode = code - min;

			return tmpCode;
		}

		/**
		 * Получение порядкового номера кода начала алфавита.
		 * @param  {Integer} code - UTF8 код
		 * @return {Integer}
		 *
		 * @public
		 */
		public int getAlpStart(int code)
		{
			int min;
			int tmpCode;

			bool isAvailable = false;
			bool isEng = false;
			bool isBig = false;

			isAvailable = _isAvailable(code);

			// Недопустимый символ
			if (!isAvailable) { return code; };

			isEng = _isEng(code);
			isBig = _isBig(code);

			// Начало и конец
			if (isEng)
			{
				if (isBig)
				{
					min = ENG_U_START;
				}
				else {
					min = ENG_L_START;
				}
			}
			else {
				if (isBig)
				{
					min = RUS_U_START;
				}
				else {
					min = RUS_L_START;
				}
			}

			return min;
		}

		/**
		 * Получение порядкового номера кода конца алфавита.
		 * @param  {Integer} code - UTF8 код
		 * @return {Integer}
		 *
		 * @public
		 */
		public int getAlpEnd(int code)
		{
			int max;
			int tmpCode;

			bool isAvailable = false;
			bool isEng = false;
			bool isBig = false;

			isAvailable = _isAvailable(code);

			// Недопустимый символ
			if (!isAvailable) { return code; };

			isEng = _isEng(code);
			isBig = _isBig(code);

			// Начало и конец
			if (isEng)
			{
				if (isBig)
				{
					max = ENG_U_END;
				}
				else {
					max = ENG_L_END;
				}
			}
			else {
				if (isBig)
				{
					max = RUS_U_END;
				}
				else {
					max = RUS_L_END;
				}
			}

			return max;
		}
	}

	interface ICrypt
	{
		string Crypt(string text, string param);
		string Decrypt(string text, string param);
	}
}

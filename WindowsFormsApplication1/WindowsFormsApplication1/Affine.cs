﻿using System;

namespace Cryptography
{
	class Affine : ICrypt
	{
		private Util util = new Util();

		/**
		 * Кодирование шифром
		 * @param  {String}     str
		 * @return {String}
		 *
		 * @public
		 */
		public string encode(string str)
		{
			int code;
			char tmpChar;
			int shift;
			int startCode;

			string endStr = "";
			char[] buffer = str.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				code = (int)buffer[i];

				startCode = util.getAlpStart(code);

				shift = util.getCharShift(code);
				shift = 3 * shift + 4;

				tmpChar = Convert.ToChar(util.shift(startCode, shift));
				endStr += tmpChar;
			}

			return endStr;
		}

		/**
		 * Декодирование шифром
		 * @param  {String}     str
		 * @return {String}
		 *
		 * @public
		 */
		public string decode(string str)
		{
			int code;
			char tmpChar;
			int shift;
			int startCode;

			string endStr = "";
			char[] buffer = str.ToCharArray();

			for (int i = 0, len = str.Length; i < len; i++)
			{
				code = (int)buffer[i];

				startCode = util.getAlpStart(code);

				shift = util.getCharShift(code);
				shift = (shift - 4) * 9;

				tmpChar = Convert.ToChar(util.shift(startCode, shift));
				endStr += tmpChar;
			}

			return endStr;
		}

		public string Crypt(string text, string param)
		{
			return encode(text);
		}
		public string Decrypt(string text, string param)
		{
			return decode(text);
		}
	}
}
